from django import forms
from .models import Product

class ProductForm(forms.ModelForm):
    title = forms.CharField(label="Title", required=True, max_length=100, widget=forms.TextInput(attrs={
        "placeholder": "Product's Title"
    }))
    descriptions = forms.CharField(widget=forms.Textarea(attrs={
            'class': 'text-area',
            'id': 'text-area',
            'rows': '10',
            'cols': '20'
        }
    ))
    price = forms.DecimalField()
    class Meta:
        model=Product
        fields=[
            'title',
            'descriptions',
            'price'
        ]
    def clean_title(self, *args, **kwargs):
        title = self.cleaned_data.get("title")
        return title
            # raise forms.ValidationError("The title is not valid")

class RowProductForm(forms.Form):
    title=forms.CharField(label="Product's Title", required=True, max_length=100,widget=forms.TextInput(attrs={
        "placeholder":"Product's Title"
    }))
    descriptions=forms.CharField(widget=forms.Textarea(
        attrs={
            'class':'text-area',
            'id':'text-area',
            'rows':'10',
            'cols':'20'
        }
    ))
    price=forms.DecimalField()