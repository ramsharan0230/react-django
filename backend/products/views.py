from django.shortcuts import render, get_object_or_404, Http404, redirect
from .models import Product
from .form import ProductForm, RowProductForm
from django.urls import reverse_lazy
# Create your views here.
# def product_create_view(request):
#     product_form = RowProductForm(request.GET)
#     if request.method == 'POST':
#         product_form = RowProductForm(request.POST)
#         if product_form.is_valid():
#             Product.objects.create(**product_form.cleaned_data)
#         else:
#             print(product_form.errors)
#
#     context = {
#         "product_form":product_form
#     }
#     return render(request, 'product/create.html', context)
def products_view(request):
    product = {
        'product':Product.objects.all()
    }
    return render(request, 'product/index.html', product)

def product_create_view(request):
    form = ProductForm(request.POST or None)
    if form.is_valid():
        form.save()
        form = ProductForm()
    context = {
        'form': form
    }
    return render(request, 'product/create.html', context)

def product_details_view(request, id):
    obj = get_object_or_404(Product, id=id)
    # try:
    #     obj = Product.objects.get(id=id)
    # except Product.DoesNotExist:
    #     raise Http404

    object = {
        'product':obj
    }
    return render(request, 'product/detail.html', object)

def product_delete_view(request, id):
    obj = get_object_or_404(Product, id=id)
    if request.method == "POST":
        print("erwe")
        obj.delete()
        return redirect('products')
    object = {
        'product': obj
    }
    return render(request, "product/delete.html", object)
